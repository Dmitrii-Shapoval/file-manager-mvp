# File manager MVP(minimum viable product)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), [Bootstrap v5]()
 and [Dropbox for JavaScript](https://www.dropbox.com/developers/documentation/javascript#overview).

At this stage of development, this file manager can
perform the following file manipulations on a remote server:

* Navigate folders on a remote Dropbox server
* Delete
* Renaming
* Moving
* Copy
* File upload
* Moving a group of files
* Copy a group of files
* Delete group of files
* Create new folder

Supports razreniye screens of computers and tablets.

## Getting the Code

To get a local copy of the current code, clone it using git:

### `git clone https://Dmitrii-Shapoval@bitbucket.org/Dmitrii-Shapoval/file-manager-mvp.git`

Next, install Node.js via the official package or via nvm. 
If you already have Node.js installed, make sure its version is > v10.
In the project directory, you need run:

### `npm install`

Then, you can run:

### `npm start`

Or if you are using yarn, then you can run.

### `yarn start`


Runs the app in the development mode.\
If the application does not start automatically in the browser, open [http://localhost:3000](http://localhost:3000) to view it in the browser.




## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

Сheck out the [Node.js](https://nodejs.org/).

