import './App.css';
import Layout from "./hoc/Layout/Layout";
import FileManager from "./containers/FileManager/FileManager";

function App() {
  return (
    <Layout>
      <FileManager/>
    </Layout>
  );
}

export default App;
