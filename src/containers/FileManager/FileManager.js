// import classes from './FileManager.module.css';
// import classes from "../../components/FilesListFolder/FileListItem/FileListItem.module.css";
import React, {Component} from "react";
import { Dropbox } from "dropbox";
import FilesListFolder from "../../components/FilesListFolder/FilesListFolder";
import Toolbar from "../../components/Toolbar/Toolbar";
import ModalWindow from "../../components/ModalWindow/ModalWindow";
import Spinner from "../../components/Spinner/Spinner";
import ModalUpload from "../../components/ModalUpload/ModalUpload";
import ModalRenaming from "../../components/ModalRenaming/ModalRenaming";
import ModalFolderCreation from "../../components/ModalFolderCreation/ModalFolderCreation"

let PropsOfSelectedFiles = {};
const dbx = new Dropbox({
    accessToken: 'W6i7QMw1t30AAAAAAAAAAa_QGvHcb2T-bqpUfx04PZ20oykAO_OfNLhkbfHQTU3_',
    // fetch
});


class FileManager extends Component {

    state = {
        FilesListOpened: [],
        id: 1,
        arrayPathFromFolder: null,
        stringPathFromFolder: null,
        currentFolderPath: "",
        allPropsOfSelectedFiles: {},
        behaviorStyles: {
            borderTable:'',
            borderBtnSet: '',
            buttonActive: 'disabled'
        },
        renamedFileProps: {},
        newFolderName: "",
        changeName: "",
        copied: false,
        cut: false,
        copy: false,
        uploadFile: null,
        selectedAll: false,
        loading: false,
    };

    // filesDownload = async (path_lower) => {
    //     console.log(path_lower);
    //     await dbx.filesDownload({path: path_lower});
    //
    // };

    getCurrentFileList = async () => {
        let filesListGot = await dbx.filesListFolder({path: this.state.currentFolderPath});
        this.setState(
          {
              FilesListOpened: filesListGot.result.entries,
          }
        )
    };

    openCreateFolderModal = async () => {
        this.setState(
          {
              newFolderName: "",
          }
        )
    };

    createNewFolder = async (event) => {
        this.setState(
          {
              newFolderName: event.target.value,
          }
        )
    };

    confirm = async () => {
        const currentPath = this.state.currentFolderPath;
        const userName = this.state.newFolderName;
        await dbx.filesCreateFolderV2({path: currentPath +'/'+ userName, autorename: true});
        await this.getCurrentFileList();
        this.setState(
          {
              newFolderName: "",
          });
    };

    fileRename = async (event) => {
        this.setState(
          {
              changeName: event.target.value,
          }
        )
    };

    submit = async () => {
        const fileProps = this.state.renamedFileProps;
        const currentPath = this.state.currentFolderPath;
        const userName = this.state.changeName;
        await dbx.filesMoveV2({from_path: fileProps.path_lower, to_path: currentPath +'/'+ userName});
        await this.getCurrentFileList();
        this.setState(
          {
              changeName: "",
          }
        )
    };

    getOldFilename = (propFile) => {
        this.setState(
          {
              changeName: propFile.name,
              renamedFileProps: propFile,
          }
        )
    };

    copyFile = async (item) => {
        await this.clearListOfSelectedFiles();
        PropsOfSelectedFiles[item.name] = item;
        this.setState(
          {
              allPropsOfSelectedFiles: PropsOfSelectedFiles
          }
        );
        this.filesCopy();
    };

    cutFile = async (item) => {
        await this.clearListOfSelectedFiles();
        PropsOfSelectedFiles[item.name] = item;
        this.setState(
          {
              allPropsOfSelectedFiles: PropsOfSelectedFiles
          }
        );
        this.filesCut()
    };

    filesCopy = async () => {
        this.setState({
            cut: false,
            copy: true,
            copied: true
        })
    };

    filesCut = async () => {
        this.setState({
            cut: true,
            copy: false,
            copied: true
        })
    };

    filesPast = async () => {
        this.setState({
            loading: true,
        });
        const allPropsOfSelectedFiles = this.state.allPropsOfSelectedFiles;
        const selectedFolderPath = this.state.currentFolderPath;
        const arrayPaths = Object.keys(allPropsOfSelectedFiles).map((propsValue) => {
            return  {
                from_path: allPropsOfSelectedFiles[propsValue].path_lower,
                to_path: selectedFolderPath +'/'+ allPropsOfSelectedFiles[propsValue].name
            }
        });
        if (this.state.copy) {
            await dbx.filesCopyBatchV2({entries: arrayPaths, autorename: true}).then(async (item) => {
                const copingFile = setInterval(async () => {
                    const status = await dbx.filesCopyBatchCheckV2({async_job_id: item.result.async_job_id});
                    if (status.result[".tag"] === "complete") {
                        this.getCurrentFileList();
                        clearInterval(copingFile);
                        this.setState({
                            loading: false,
                        })
                    }
                }, 300);
                this.setState(
                  {
                      arrayPaths: [],
                      allPropsOfSelectedFiles: {}
                  }
                )
            });
        }
        if (this.state.cut) {
            debugger;
            await dbx
            .filesMoveBatchV2({
                entries: arrayPaths,
                autorename: true,
                allow_ownership_transfer: true
            }).then(async (item) => {
                  const movingFile = setInterval(async () => {
                      const status = await dbx.filesMoveBatchCheckV2({async_job_id: item.result.async_job_id});
                      if (status.result[".tag"] === "complete") {
                          this.getCurrentFileList();
                          clearInterval(movingFile);
                          this.setState({
                              loading: false,
                          })
                      }
                  }, 300);
                  this.setState(
                    {
                        arrayPaths: [],
                        allPropsOfSelectedFiles: {}
                    }
                  )
            });
        }
        this.clearListOfSelectedFiles()
    };

    clearListOfSelectedFiles = async () => {
        PropsOfSelectedFiles = {};
        await this.getCurrentFileList();
        this.setState({
            allPropsOfSelectedFiles: PropsOfSelectedFiles,
            behaviorStyles: {
                borderTable:'',
                borderBtnSet:'',
                buttonActive: 'disabled'
            }
        })
    };

    selectedAllFiles = async () => {
        if(this.state.selectedAll) {
            PropsOfSelectedFiles = {};
            this.setState({
                selectedAll: false,
                behaviorStyles: {
                    borderTable:'',
                    borderBtnSet:'',
                    buttonActive: 'disabled'
                }
            })
        } else {
            this.state.FilesListOpened.map(item => {
                PropsOfSelectedFiles[item.name] = item;
            });
            this.setState({
                selectedAll: true,
                behaviorStyles: {
                    borderTable:'border-primary',
                    borderBtnSet: 'border-danger',
                    buttonActive: ''
                }
            })
        }
        this.setState(
          {
              allPropsOfSelectedFiles: PropsOfSelectedFiles
          }
        );
    };

    WritePropsOfSelectedFiles = async (event, propsOfOneFile, fileName) => {
        if (event.target.checked) {
            PropsOfSelectedFiles[fileName] = propsOfOneFile;
        } else {
            delete PropsOfSelectedFiles[fileName]
        }
        this.getCurrentFileList();
        this.setState(
            {
                allPropsOfSelectedFiles: PropsOfSelectedFiles
            }
        );
        if (Object.keys(this.state.allPropsOfSelectedFiles).length) {
            this.setState({
                behaviorStyles: {
                    borderTable:'border-primary',
                    borderBtnSet: 'border-danger',
                    buttonActive: ''
                }
            })
        } else {
            this.setState({
                behaviorStyles: {
                    borderTable:'',
                    borderBtnSet:'',
                    buttonActive: 'disabled'
                }
            })
        }
    };

    changeUploadFile = (event) => {
        const file = event.target.files[0];
        this.setState({
            uploadFile: file,
        })
    };


    uploadFile = async () => {
        let file = this.state.uploadFile;
        const UPLOAD_FILE_SIZE_LIMIT = 150 * 1024 * 1024;
        if (file.size < UPLOAD_FILE_SIZE_LIMIT) { // File is smaller than 150 Mb - use filesUpload API
            try {
                await dbx.filesUpload({path: this.state.currentFolderPath + '/' + file.name, contents: file});
            } catch (e) {
                console.error('Error');
            } finally { }
        } else { // File is bigger than 150 Mb - use filesUploadSession* API
                const maxBlob = 8 * 1000 * 1000; // 8Mb - Dropbox JavaScript API suggested max file / chunk size
                const workItems = [];
                let offset = 0;
                while (offset < file.size) {
                    let chunkSize = Math.min(maxBlob, file.size - offset);
                    workItems.push(file.slice(offset, offset + chunkSize));
                    offset += chunkSize;
                }
                const task = workItems.reduce((acc, blob, idx, items) => {
                    if (idx === 0) {
                        // Starting multipart upload of file
                        return acc.then(function() {
                            return dbx.filesUploadSessionStart({ close: false, contents: blob})
                                .then(response => response.session_id)
                        });
                    } else if (idx < items.length-1) {
                        // Append part to the upload session
                        return acc.then(function(sessionId) {
                            const cursor = { session_id: sessionId, offset: idx * maxBlob };
                            return dbx.filesUploadSessionAppendV2({ cursor: cursor, close: false, contents: blob }).then(() => sessionId);
                        });
                    } else {
                        // Last chunk of data, close session
                        return acc.then(function(sessionId) {
                            const cursor = { session_id: sessionId, offset: file.size - blob.size };
                            const commit = { path: '/' + file.name, mode: 'add', autorename: true, mute: false };
                            return dbx.filesUploadSessionFinish({ cursor: cursor, commit: commit, contents: blob });
                        });
                    }
                }, Promise.resolve());

                task.then(function(result) {
                    console.log('File uploaded!');
                }).catch(function(e) {
                    console.error(e);
                });
        }
        await this.getCurrentFileList();
        return false;
    };

    removeSelected =  async () => {
        const allPropsOfSelectedFiles = this.state.allPropsOfSelectedFiles;
        debugger;
        const arrayPaths = Object.keys(allPropsOfSelectedFiles).map((propsValue) => {
            return  {
                name: allPropsOfSelectedFiles[propsValue].path_lower,
            }
        });
        await Promise.all(arrayPaths.map(async (path) => {
            this.deleteListItem(path.name)
        }));
        this.clearListOfSelectedFiles();
    };

    deleteListItem = async (path) => {
        let filesDelete = await dbx.filesDeleteV2({path: path});
        console.log(filesDelete);
        try {
            console.log('Promise got');
            this.getCurrentFileList();
        } catch (e) {
            console.error('Error');
        } finally { }
    };

    openFolder = async (pathLower, pathDisplay) => {
        this.setState({FilesListOpened: null});
        try {
            console.log('Promise got');
            const openFolder = await dbx.filesListFolder({path: pathLower});
            const arrayPathFromFolder = pathLower.split('/');
            // this.setState({FilesListOpened: null});
            this.setState(
                {
                    FilesListOpened: openFolder.result.entries,
                    arrayPathFromFolder: arrayPathFromFolder,
                    currentFolderPath: pathLower
                    }
                )
            } catch (e) {
                console.error('Error');
            } finally {
                // console.log('Path:', this.state.arrayPathFromFolder);
            }
    };

    exitFolder = async (arrayPathFrom = this.state.arrayPathFromFolder) => {
        // this.setState({FilesListOpened: null});
        let newPathFromFolder = [];
        if (arrayPathFrom) {
            for (let i = 1; arrayPathFrom.length-2 >= i; i++) {
                newPathFromFolder.push('/', arrayPathFrom[i]);
            }
        }
        try {
            console.log('Promise got');
            const stringPathFromFolder = newPathFromFolder.join('');
            const openFolder = await dbx.filesListFolder({path: stringPathFromFolder});
            this.setState(
                {
                    FilesListOpened: openFolder.result.entries,
                    arrayPathFromFolder: stringPathFromFolder.split('/'),
                    stringPathFromFolder: stringPathFromFolder,
                    currentFolderPath: stringPathFromFolder
                }
            )
        } catch (e) {
            console.error('Error');
        } finally {
            // console.log('Path:', this.state.arrayPathFromFolder);
        }
    };

    async componentDidMount() {
        try {
            console.log('Promise got');
            const filesListGot = await dbx.filesListFolder({path: ''});
            this.setState(
                {
                    FilesListOpened: filesListGot.result.entries,
                }
            )
        } catch (e) {
            console.error('Error');
        } finally { }
    }

    render() {
        return (
            <div className={"container-sm"}>
                <header>
                    <Toolbar
                      enableCope={Object.keys(this.state.allPropsOfSelectedFiles).length > 0}
                      enablePast={this.state.copied}
                      removeSelected={this.removeSelected}
                      filesPast={this.filesPast}
                      filesCopy={this.filesCopy}
                      filesCut={this.filesCut}
                      styles={this.state.behaviorStyles}
                      removeSelection={this.clearListOfSelectedFiles}
                      openCreateFolderModal={this.openCreateFolderModal}
                      loading={this.state.loading}
                    />
                </header>
                <main>
                    {this.state.FilesListOpened ?
                        <FilesListFolder
                            selectedAllFiles={this.selectedAllFiles}
                            deleteListItem={this.deleteListItem}
                            openFolder={this.openFolder}
                            exitFolder={this.exitFolder}
                            allFileProperties={this.state.FilesListOpened}
                            selectFiles={this.WritePropsOfSelectedFiles}
                            filesDownload={this.filesDownload}
                            getOldFilename={this.getOldFilename}
                            styles={this.state.behaviorStyles}
                            PropsOfSelectedFiles={PropsOfSelectedFiles}
                            copyFile={this.copyFile}
                            cutFile={this.cutFile}
                        />
                        : <Spinner/>
                    }
                </main>
                <ModalWindow/>
                <ModalRenaming
                    fileRename={this.fileRename}
                    rename={this.submit}
                    name={this.state.changeName}
                />
                <ModalUpload
                    changeUploadFile={this.changeUploadFile}
                    uploadFile={this.uploadFile}
                />
                <ModalFolderCreation
                    createNewFolder={this.createNewFolder}
                    confirm={this.confirm}
                    newFolderName={this.state.newFolderName}
                />
            </div>
        )
    }
}

export default FileManager
