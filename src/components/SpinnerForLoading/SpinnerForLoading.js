import React from 'react';
import classes from './SpinnerForLoading.module.css'

const SpinnerForLoading = props => {
    return (
        <div className={`${classes.SpinnerForLoading} d-flex align-items-center text-light col-3`}>
            <div className="spinner-border ms-auto m-3" role="status" aria-hidden="true"></div>
            <strong> Loading... </strong>
        </div>
    )
};

export default SpinnerForLoading