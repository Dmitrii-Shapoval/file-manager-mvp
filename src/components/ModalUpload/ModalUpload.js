import React from "react";


const ModalUpload = props => {
    return (
            <div className="modal fade" id="modalUpload" tabIndex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Select a file to upload</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close">
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="mb-3 d-flex align-items-center">
                                <input className="form-control m-2" type="file" onChange={(event) => props.changeUploadFile(event)}/>
                                <button type="submit" className="btn btn-primary m-2" data-bs-dismiss="modal" onClick={props.uploadFile}>Upload</button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
    )
};


export default ModalUpload
