// import classes from './DropdownMenu.module.css';
import React from 'react';

const DropdownMenu = props => {
    return (
        <div className="btn-group dropdown col-10" ref={props.dropdownRef}>
            <button type="button" className={`${props.btnHidden} btn btn-secondary dropdown-toggle btn-sm bg-dark border-dark`}
                    data-bs-toggle="dropdown"
                    aria-expanded="false">
            </button>

            <ul className="dropdown-menu bg-dark border border-secondary">
            <li className="">
                <button type="button" className="btn btn-outline-light btn-sm col-12 border-dark"
                    onClick={() => props.cutFile(props.propsOfOneFile)}>Cut</button>
            </li>
            <li className="">
                <button type="button" className="btn btn-outline-light btn-sm col-12 border-dark"
                    onClick={() => props.copyFile(props.propsOfOneFile)}>Copy</button>
            </li>
            <li className="">
                <button type="button" className="btn btn-outline-light btn-sm col-12 border-dark"
                    data-bs-toggle="modal" data-bs-target="#modalRenaming" onClick={() => props.getOldFilename(props.propsOfOneFile)}>Rename</button>
            </li>
            <li className="">
                <button type="button" className="btn btn-outline-light btn-sm col-12 border-dark
                    text-danger" onClick={() => props.deleteListItem(props.propsOfOneFile.path_lower)}>Delete</button>
            </li>
            {/*<li className="">
                <button type="button" className="btn btn-outline-light btn-sm col-12 border-dark"
                        onClick={() => props.filesDownload(props.propsOfOneFile.path_lower)}>Download</button>
            </li>*/}
            </ul>
        </div>
    )
};

export default DropdownMenu
