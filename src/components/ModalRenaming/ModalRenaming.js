import React from "react";

const ModalUpload = props => {
    return (
        <div className="modal fade" id="modalRenaming" tabIndex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-md">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Enter a new filename with an extension</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal"
                                aria-label="Close">
                        </button>
                    </div>
                    <div className="modal-body">
                        <form className="mb-3 d-flex align-items-center">
                            <input className="form-control m-2" type="text" value={props.name} onChange={(event) => props.fileRename(event)}/>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" data-bs-dismiss="modal">Close</button>
                        <button type="button" className="btn" data-bs-dismiss="modal" onClick={props.rename}>Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ModalUpload
