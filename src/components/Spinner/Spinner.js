import React from 'react';
import classes from './Spinner.module.css'

const Spinner = props => {
  return (
    <div className={`${classes.Spinner} d-flex align-items-center text-light w-50`}>
        <div className="spinner-border ms-auto m-3" role="status" aria-hidden="true"></div>
        <strong> Loading...</strong>
    </div>
  )
};

export default Spinner