// import classes from './FilesListFolder.module.css';
import React from "react";
import FileListItem from "./FileListItem/FileListItem";

const FilesListFolder = props => (
        <table className={`table table-dark table-hover ${props.styles.borderTable}`}>
            <thead>
            <tr>
                <th scope="col" className="align-middle">
                    <i className="bi bi-file-earmark-check classes.check" onClick={props.selectedAllFiles}></i>
                </th>
                <th scope="col" className="col-5">File name</th>
                <th scope="col" className="col-2">Size (KB)</th>
                <th scope="col" className="col-3">Date modified (created)</th>
                <th scope="col" className="col-1">
                    <button type="button" className="btn btn-outline-light btn-sm col-10 border-secondary" onClick={() => props.exitFolder()}>
                        <i className="bi bi-arrow-90deg-left"></i>
                    </button>
                </th>
            </tr>
            </thead>
            <tbody>
            {props.allFileProperties.map((item, index) => {
                return (
                    <FileListItem
                        key={index}
                        propsOfOneFile={item}
                        deleteListItem={props.deleteListItem}
                        openFolder={props.openFolder}
                        selectFiles={props.selectFiles}
                        filesDownload={props.filesDownload}
                        getOldFilename={props.getOldFilename}
                        PropsOfSelectedFiles={props.PropsOfSelectedFiles}
                        copyFile={props.copyFile}
                        cutFile={props.cutFile}
                    />
                );
            })}
            </tbody>
        </table>
);

export default FilesListFolder
