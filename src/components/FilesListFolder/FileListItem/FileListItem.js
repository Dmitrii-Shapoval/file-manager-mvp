// import DropdownMenu from "../../DropdownMenu/DropdownMenu";
import React from "react";
import classes from "./FileListItem.module.css";
import ColumnCell from "./СolumnСell/ColumnСell";
import classesBtn from "../../DropdownMenu/DropdownMenu.module.css";

const FileListItem = (props, state) => {
    const [btnHidden, changeButtonHidden] = React.useState(classesBtn.btnHidden__closed);
    const dropdownRef = React.useRef("");
    const formattedDate = (date = props.propsOfOneFile.server_modified) => {
        if (date) {
            return `${date.substring(0, 10)}\u00A0\u00A0[${date.substring(11, 19)}]`
        }
    };
    const active = !!Object.keys(props.PropsOfSelectedFiles).find(
        item => props.PropsOfSelectedFiles[item].path_lower === props.propsOfOneFile.path_lower
    );
    return (
        <tr className={`${classes.rowFromList} rowFromListShow align-middle`}
            onMouseEnter={() => changeButtonHidden("")}
            onDoubleClick={() => props.propsOfOneFile[".tag"] === "folder"
                ? props.openFolder(props.propsOfOneFile.path_lower, null)
                : null}
            onMouseLeave={() => {dropdownRef && dropdownRef.current && dropdownRef.current.click();
                changeButtonHidden(classesBtn.btnHidden__closed)}}
        >
            <th>
                <input className="form-check-input" type="checkbox" checked={active} id="flexCheckDefault"
                       onClick={(event) => props.selectFiles(event, props.propsOfOneFile, props.propsOfOneFile.name)}/>
            </th>
            <ColumnCell
                propsOfOneFile={props.propsOfOneFile}
                fileModificationDate={formattedDate()}
                btnHidden={btnHidden}
                deleteListItem={props.deleteListItem}
                filesDownload={props.filesDownload}
                getOldFilename={props.getOldFilename}
                dropdownRef={dropdownRef}
                copyFile={props.copyFile}
                cutFile={props.cutFile}
            />
        </tr>
    )
};

export default FileListItem
