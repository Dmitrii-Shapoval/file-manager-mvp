import React from 'react';
import classes from "../FileListItem.module.css";
import DropdownMenu from "../../../DropdownMenu/DropdownMenu";

const ColumnCell = (props) => {
    return(
        <>
            <td className={`${classes.truncatText} text-truncate`}>
                {props.propsOfOneFile[".tag"] === "folder"
                ? <i className="bi bi-folder"> </i>
                : <i className="bi bi-file-earmark-fill"> </i>}
                {props.propsOfOneFile.name}
            </td>
            <td>
                {props.propsOfOneFile.size
                ? (props.propsOfOneFile.size/1024).toFixed(2) + ' KB'
                : null}
            </td>
            <td>
                {props.fileModificationDate}
            </td>
            <td>
                <DropdownMenu
                    btnHidden={props.btnHidden}
                    propsOfOneFile={props.propsOfOneFile}
                    deleteListItem={props.deleteListItem}
                    filesDownload={props.filesDownload}
                    dropdownRef={props.dropdownRef}
                    getOldFilename={props.getOldFilename}
                    copyFile={props.copyFile}
                    cutFile={props.cutFile}
                />
            </td>
        </>
    )
};

export default ColumnCell
