import React from "react";
import classes from "./Toolbar.module.css";
import SpinnerForLoading from "../SpinnerForLoading/SpinnerForLoading";

const Toolbar = props => {
    return (
            <>
                <div className="collapse show" id="navbarToggleExternalContent">
                    <div className="bg-dark p-4">
                        <h1 className="text-white h1">File manager MVP</h1>
                        <span className="text-muted">Extended toolbar menu V1.0</span>
                    </div>
                </div>

                <nav className="navbar navbar-dark bg-dark">
                    <div className="container-fluid">
                        <a href="#" className={props.loading ? 'col-7' : 'col-10'}>
                            <i className={"bi bi-folder2-open"}></i>
                        </a>
                        {props.loading && <SpinnerForLoading />}
                        <div className="dropdown align-content-center col-1">
                            <button className={`btn btn-dark dropdown-toggle ${props.styles.borderBtnSet}`} type="button" id="dropdownMenuButton2"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                <i className="bi bi-tools"></i>
                            </button>
                            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                                <li><a className={`dropdown-item ${props.enableCope ? '' : 'disabled'}`} href="#" onClick={() => {props.filesCopy()}}>Copy</a></li>
                                <li><a className={`dropdown-item ${props.enablePast ? '' : 'disabled'}`} href="#" onClick={() => props.filesPast()}>Past</a></li>
                                <li><a className={`dropdown-item ${props.enableCope ? '' : 'disabled'}`} href="#" onClick={() => {props.filesCut()}}>Cut</a></li>
                                <li><a className="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">Action</a></li>
                                <li><a className={`dropdown-item ${props.enableCope ? '' : 'disabled'}`} href="#" onClick={() => props.removeSelected()}>Delete selected</a></li>
                                <li><a className="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#modalFolderCreation" onClick={props.openCreateFolderModal}>Create a new folder</a></li>
                                <li><hr className="dropdown-divider"/></li>
                                <li><a className={`dropdown-item ${props.styles.buttonActive}`} href="#" onClick={() => props.removeSelection()}> Remove all selections</a></li>
                                <li><hr className="dropdown-divider"/></li>
                                <li><a className="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#modalUpload">Upload file</a></li>
                            </ul>
                        </div>

                        <button className="navbar-toggler btn-sm border border-dark col-1" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarToggleExternalContent"
                                aria-controls="navbarToggleExternalContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </nav>
            </>
    )
};

export default Toolbar




